﻿using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace LiveChat.Serialization
{
    public class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}
