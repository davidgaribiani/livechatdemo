﻿namespace LiveChat.Models
{
    public class SendMessageInputVM
    {
        public string Content { get; set; }
    }
}
