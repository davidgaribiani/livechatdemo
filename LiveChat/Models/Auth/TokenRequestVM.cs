﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace LiveChat.Models.Auth
{
    public class TokenRequestVM
    {
        [Required]
        [JsonProperty("token")]
        public string Token { get; set; }

        [Required]
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }

}
