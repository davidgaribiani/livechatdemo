﻿using System.ComponentModel.DataAnnotations;

namespace LiveChat.Models.Auth
{
    public class LoginVM
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
