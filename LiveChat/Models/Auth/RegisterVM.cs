﻿using Duende.IdentityServer.Models;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace LiveChat.Models.Auth
{
    public class RegisterVM
    {
        [JsonProperty("firstName")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "FirstName is required")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "LastName is required")]
        public string LastName { get; set; }

        [JsonProperty("userName")]
        [Required(ErrorMessage = "UserName is required")]
        [StringLength(16, ErrorMessage = "Must be between 3 and 16 characters", MinimumLength = 3)]
        public string UserName { get; set; }

        [JsonProperty("email")]
        [Required(ErrorMessage = "Email is required")]
        [StringLength(50, ErrorMessage = "Must be between 5 and 50 characters", MinimumLength = 5)]
        [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Must be a valid email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        [Required(ErrorMessage = "Password is required")]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [JsonProperty("confirmPassword")]
        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        //[JsonProperty("acceptTerms")]
        //[Required]
        //[Range(typeof(bool), "true", "true", ErrorMessage = "Accepting terms is required")]
        //public bool AcceptTerms { get; set; }

    }
}
