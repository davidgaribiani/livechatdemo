﻿// create connection
var connectioUserCount = new signalR
    .HubConnectionBuilder()
    .withUrl("/hubs/chat")
    .build();

// connect to methods that hub invokes aka receive notifications from hub
connectioUserCount.on("renderMessage", (newMessage) => {
    var messages = document.getElementById("messages");

    const date = moment(newMessage.timeSent);;
    newMessage.timeSent = date.format('M/DD/yyyy hh:mm:ss A');

    let html = `       
        <div class='msg'> 
            <div class='msg-header''>${newMessage.senderName}</div>
            <div class='msg-body'>${newMessage.content}</div>
            <div class='msg-footer'>${newMessage.timeSent}</div>
        </div>
        `;

    messages.insertAdjacentHTML("afterbegin", html);
})

connectioUserCount.on("updateTotalViews", (value) => {
    var counter = document.getElementById("totalViewsCounter");
    counter.innerText = value.toString();
})

connectioUserCount.on("updateTotalUsers", (value) => {
    var counter = document.getElementById("totalUsersCounter");
    counter.innerText = value.toString();
})

// invoke hub methods aka send notification to hub
function newWindowLoadedOnClient() {
    connectioUserCount.send("NewWindowLoaded");
}

// start connection
function fulfilled() {
    // do something on start
    newWindowLoadedOnClient();
}

function rejected() {
    // rejected logs
    console.log("!connection_rejected");
}


connectioUserCount.start().then(fulfilled, rejected);


