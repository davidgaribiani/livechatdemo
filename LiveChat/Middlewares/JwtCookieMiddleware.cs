﻿    using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

namespace LiveChat.Middlewares
{
    public class JwtCookieMiddleware
    {
        private readonly RequestDelegate _next;

        public JwtCookieMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var jwtCookie = context.Request.Cookies["Bearer"];
            if (jwtCookie != null)
            {
                var jwtHandler = new JwtSecurityTokenHandler();
                var jwtToken = jwtHandler.ReadJwtToken(jwtCookie);

                var claimsIdentity = new ClaimsIdentity(jwtToken.Claims, "jwt");
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                var authenticationProperties = new AuthenticationProperties();
                var authenticationTicket = new AuthenticationTicket(
                    new ClaimsPrincipal(claimsIdentity),
                    authenticationProperties,
                    "jwt");

                //await context.SignInAsync("jwt", authenticationTicket);

                context.Request.Headers.TryAdd("Authorization", $"Bearer {jwtCookie}");


                // await context.SignInAsync(JwtBearerDefaults.AuthenticationScheme, claimsPrincipal, authenticationProperties);
            }

            await _next(context);
        }
    }

    public static class JwtCookieMiddlewareExtensions
    {
        public static IApplicationBuilder UseJwtCookieMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<JwtCookieMiddleware>();
        }
    }

}
