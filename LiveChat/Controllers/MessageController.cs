﻿using LiveChat.Hubs;
using LiveChat.Models;
using LiveChat.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace LiveChat.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly MessageService _messageService;
        private readonly IHubContext<ChatHub> _hubContext;

        public MessagesController(MessageService messagingService, IHubContext<ChatHub> hubContext)
        {
            _messageService = messagingService;
            _hubContext = hubContext;
        }

        [HttpGet]
        public async Task<IActionResult> RetrieveMessages(CancellationToken cancellationToken)
        {
            var result = await _messageService.GetMessagesAsync(cancellationToken);
            return StatusCode(200, result);
        }

        [HttpPost(Name = "send-message")]
        public async Task<IActionResult> SendMessage([FromForm]SendMessageInputVM vm, CancellationToken cancellationToken)
        {
            var message = await _messageService.SendMessageAsync(vm, cancellationToken);
            await _hubContext.Clients.All.SendAsync("renderMessage", message, cancellationToken);
            //return StatusCode(200, message.Id);
            return NoContent();
        }
    }
}
