﻿using LiveChat.Data;
using LiveChat.Data.Entities.Identity;
using LiveChat.Data.Entities.Identity.Token;
using LiveChat.Data.Helpers;
using LiveChat.Extensions;
using LiveChat.Models;
using LiveChat.Models.Auth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace LiveChat.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly TokenValidationParameters _tokenValidationParameters;
        private readonly ILogger<AuthController> _logger;

        public AuthController(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            ApplicationDbContext context,
            IConfiguration configuration,
            TokenValidationParameters tokenValidationParameters,
            ILogger<AuthController> logger)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _configuration = configuration;
            _tokenValidationParameters = tokenValidationParameters;
            _logger = logger;
        }

        #region Action Methods Returning Views

        [HttpGet("/login", Name = "login")]
        public IActionResult ReturnLoginView()
        {
            return View("Login");
        }

        [HttpGet("/logout", Name = "logout")]
        public IActionResult Logout()
        {
            foreach (var cookieKey in Request.Cookies.Keys)
            {
                Response.Cookies.Delete(cookieKey);
            }

            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpGet("/register", Name = "register")]
        public IActionResult ReturnRegisterView()
        {
            return View("Register");
        }

        #endregion


        #region Action Methods

        [HttpPost("login", Name = "login-submit")]
        public async Task<IActionResult> Login([FromForm] LoginVM loginVM)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var validationErrors = ModelState.ToDictionary(
                        kvp => kvp.Key.ToCamelCase(),
                        kvp => kvp.Value.Errors.FirstOrDefault()?.ErrorMessage);

                    return BadRequest(validationErrors);
                }


                var userExists = await _userManager.FindByNameAsync(loginVM.UserName);

                if (userExists != null && await _userManager.CheckPasswordAsync(userExists, loginVM.Password))
                {
                    var tokenValue = await GenerateJWTTokenAsync(userExists, null);

                    HttpContext.Response.Cookies.Append("Bearer", tokenValue.Token);


                    return RedirectToAction(nameof(HomeController.Index), "Home");
                    //return StatusCode(200, tokenValue);

                }

                return Unauthorized("Invalid username or password.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to sign in.");
                return StatusCode(500, "Something went wrong. Failed to login.");
            }
        }

        [HttpPost("register", Name = "register-submit")]
        public async Task<IActionResult> Register([FromForm] RegisterVM registerViewModel)
        {
            try
            {
                var validationErrors = new Dictionary<string, string>(); // key, errorMessage

                if (!ModelState.IsValid)
                {
                    foreach (var modelStateKey in ModelState.Keys)
                    {
                        var value = ModelState[modelStateKey];
                        validationErrors.Add(modelStateKey.ToCamelCase(), value.Errors.FirstOrDefault().ErrorMessage);
                    }

                    return StatusCode(400, validationErrors);
                }

                bool userWithEmailExists = _userManager.Users.Any(user => user.Email == registerViewModel.Email);
                bool userWithUserNameExists = _userManager.Users.Any(user => user.UserName == registerViewModel.UserName);

                if (userWithEmailExists || userWithUserNameExists)
                {
                    if (userWithEmailExists) validationErrors.Add(nameof(registerViewModel.Email).ToCamelCase(), "User with entered email does already exist.");
                    if (userWithUserNameExists) validationErrors.Add(nameof(registerViewModel.UserName).ToCamelCase(), "User with entered username does already exist.");

                    return StatusCode(400, validationErrors);
                }

                ApplicationUser newUser = new ApplicationUser()
                {
                    FirstName = registerViewModel.FirstName,
                    LastName = registerViewModel.LastName,
                    Email = registerViewModel.Email,
                    UserName = registerViewModel.UserName,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                var result = await _userManager.CreateAsync(newUser, registerViewModel.Password);

                if (result.Succeeded)
                {
                    // Add default role : 'Customer' to the user
                    await _userManager.AddToRoleAsync(newUser, UserRoles.User);
                    return StatusCode(200);
                }

                return StatusCode(500, "User could not be created.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to sign up.");
                return StatusCode(500, "Something went wrong. User could not be created.");
            }
        }

        [HttpPost("refresh-token")]
        public async Task<IActionResult> RefreshToken(TokenRequestVM tokenRequestViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var errors = new List<string>();

                    foreach (var error in ModelState.Values.SelectMany(modelState => modelState.Errors))
                    {
                        errors.Add(error.ErrorMessage);
                    }

                    return StatusCode(400, errors);
                }

                var result = await VerifyAndGenerateTokenAsync(tokenRequestViewModel);

                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to refresh the token.");
                return StatusCode(500);
            }
        }

        #endregion

        #region Token-Handling Helper Methods

        [NonAction]
        private async Task<AuthResultVM> VerifyAndGenerateTokenAsync(TokenRequestVM tokenRequestViewModel)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var storedToken = await _context.RefreshTokens.FirstOrDefaultAsync(rt => rt.Token ==
                tokenRequestViewModel.RefreshToken);
            var dbUser = await _userManager.FindByIdAsync(storedToken.UserId.ToString());

            try
            {
                var tokenCheckResult = jwtTokenHandler.ValidateToken(tokenRequestViewModel.Token, _tokenValidationParameters,
                    out var validatedToken);

                return await GenerateJWTTokenAsync(dbUser, storedToken);
            }
            catch (SecurityTokenExpiredException)
            {
                if (storedToken.DateExpires >= DateTime.UtcNow)
                {
                    return await GenerateJWTTokenAsync(dbUser, storedToken);
                }
                else
                {
                    return await GenerateJWTTokenAsync(dbUser, null);
                }
            }
        }

        [NonAction]
        private async Task<AuthResultVM> GenerateJWTTokenAsync(ApplicationUser user, RefreshToken refreshToken)
        {
            var authClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            // Add User Role Claims

            var userRoles = await _userManager.GetRolesAsync(user);

            foreach (var role in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role));
            }

            var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:Issuer"],
                    audience: _configuration["JWT:Audience"],
                    expires: DateTime.UtcNow.AddHours(24), // TODO AddMinutes(2)
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            if (refreshToken != null)
            {
                var response = new AuthResultVM()
                {
                    Token = jwtToken,
                    RefreshToken = refreshToken.Token,
                    ExpiresAt = token.ValidTo
                };

                return response;
            }
            else
            {
                var newRefreshToken = new RefreshToken()
                {
                    JwtId = token.Id,
                    IsRevoked = false,
                    UserId = user.Id,
                    DateAdded = DateTime.UtcNow,
                    DateExpires = DateTime.UtcNow.AddMonths(6),
                    Token = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString()
                };

                await _context.RefreshTokens.AddAsync(newRefreshToken);
                await _context.SaveChangesAsync();

                var response = new AuthResultVM()
                {
                    Token = jwtToken,
                    RefreshToken = newRefreshToken.Token,
                    ExpiresAt = token.ValidTo
                };


                // Add Jwt Token To Db
                await _context.JwtTokens.AddAsync(new UserToken()
                {
                    ValidFrom = token.ValidFrom,
                    ValidTo = token.ValidTo,
                    Value = jwtToken,
                    UserId = user.Id,
                    RefreshToken = response.RefreshToken
                });

                await _context.SaveChangesAsync();

                return response;
            }
        }

        #endregion
    }

}
