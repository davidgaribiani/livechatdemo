﻿using Microsoft.AspNetCore.SignalR;

namespace LiveChat.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage()
        {
            await Clients.All.SendAsync("");
        }
    }
}
