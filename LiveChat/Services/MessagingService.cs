﻿using LiveChat.Data;
using LiveChat.Data.Entities;
using LiveChat.Models;
using Microsoft.EntityFrameworkCore;

namespace LiveChat.Services
{
    public class MessageService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ApplicationDbContext _context;

        public MessageService(IHttpContextAccessor httpContextAccessor, ApplicationDbContext context)
        {
            _httpContextAccessor = httpContextAccessor;
            _context = context;
        }

        public async Task<Message> SendMessageAsync(SendMessageInputVM input, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(input.Content))
                throw new ArgumentOutOfRangeException(nameof(input.Content));

            var messageToSend = new Message()
            {
                Content = input.Content
            };

            if (_httpContextAccessor!.HttpContext!.User!.Identity!.IsAuthenticated)
            {
                messageToSend.SenderName = _httpContextAccessor!.HttpContext!.User!.Identity!.Name!;
            }

            await _context.Messages.AddAsync(messageToSend);
            await _context.SaveChangesAsync(cancellationToken);

            return messageToSend;
        }

        public async Task<IEnumerable<Message>> GetMessagesAsync(CancellationToken cancellationToken, bool orderByDesc = false)
        {
            var result = orderByDesc ?
                await _context.Messages.OrderByDescending(x => x.Id).ToListAsync(cancellationToken) :
                await _context.Messages.ToListAsync(cancellationToken);
            return result;
        }
    }
}
