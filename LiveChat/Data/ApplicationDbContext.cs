﻿using Duende.IdentityServer.EntityFramework.Options;
using LiveChat.Data.Entities;
using LiveChat.Data.Entities.Identity;
using LiveChat.Data.Entities.Identity.Token;
using LiveChat.Models;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace LiveChat.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<UserToken> JwtTokens { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }

        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {

        }
    }
}