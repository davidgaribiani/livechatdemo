﻿using Microsoft.AspNetCore.Identity;

namespace LiveChat.Data.Entities.Identity
{
    public class ApplicationRole : IdentityRole<Guid>
    {
    }
}
