﻿using System.ComponentModel.DataAnnotations;

namespace LiveChat.Data.Entities.Identity.Token
{
    public class UserToken
    {
        [Key]
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string Value { get; set; }

        public DateTime ValidFrom { get; set; }

        public DateTime ValidTo { get; set; }

        public bool IsRevoked { get; set; }

        public string RefreshToken { get; set; }

    }
}
