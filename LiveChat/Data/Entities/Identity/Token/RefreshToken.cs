﻿namespace LiveChat.Data.Entities.Identity.Token
{
    public class RefreshToken
    {
        public Guid Id { get; set; }

        public string Token { get; set; }

        public string JwtId { get; set; }

        public bool IsRevoked { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime DateExpires { get; set; }

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }

    }
}
