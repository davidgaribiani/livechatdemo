﻿using Microsoft.AspNetCore.Identity;

namespace LiveChat.Data.Entities.Identity
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
