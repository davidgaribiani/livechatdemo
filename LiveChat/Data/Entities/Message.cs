﻿using LiveChat.Serialization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace LiveChat.Data.Entities
{
    public class Message
    {
        public Guid Id { get; set; }

        [JsonConverter(typeof(DateFormatConverter), "M/dd/yyyy 0:hh:mm:ss tt")]
        public DateTime TimeSent { get; set; } = DateTime.Now;

        public string Content { get; set; }

        public string SenderName { get; set; } = "Anonymous";

        //public IdentityUser? User { get; set; }

        //public string? UserId { get; set; } 
    }
}
