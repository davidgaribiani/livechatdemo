﻿namespace LiveChat.Data.Helpers
{
    public static class UserRoles
    {
        public const string User = "User";
        public const string Moderator = "Moderator";
        public const string Admin = "Admin";
    }
}
